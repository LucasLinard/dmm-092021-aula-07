import 'package:json_annotation/json_annotation.dart';

part 'baby_names.g.dart';

@JsonSerializable()
class BabyName {
  @JsonKey(defaultValue: 0)
  int count;
  String name;

  BabyName({required this.count, required this.name});

  factory BabyName.fromJson(Map<String, dynamic> json) =>
      _$BabyNameFromJson(json);

  Map<String, dynamic> toJson() => _$BabyNameToJson(this);
}