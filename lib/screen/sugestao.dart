import 'package:baby_names/model/baby_names.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SugestaoScreen extends StatefulWidget {
  static const route = "/sugestao";
  static const title = "Sugestão";
  const SugestaoScreen({Key? key}) : super(key: key);

  @override
  State<SugestaoScreen> createState() => _SugestaoScreenState();
}

class _SugestaoScreenState extends State<SugestaoScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _controller = TextEditingController();
  bool _processing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(SugestaoScreen.title),
        ),
        body: Container(
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _controller,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                ),
              ),
              Visibility(
                  visible: _processing,
                  child: CircularProgressIndicator()),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Sugestão enviada')),
                    );
                    BabyName babyName = BabyName(count: 0, name: _controller.text);
                    setState(() {
                      _processing = true;
                    });
                    FirebaseFirestore.instance.collection('baby_names').add(babyName.toJson()).then((value) {
                      setState(() {
                        _processing = false;
                        _controller.text = "";
                      });
                      Navigator.of(context).pop();
                    });
                  }
                },
                child: const Text("Enviar"),),
              )
            ],
          ),
        ));
  }
}
